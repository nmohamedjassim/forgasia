<?php
require_once './dbHandler.php';
$db = new DbHandler();
$frogs = $db->getAllRecord("select * from frogs_main ORDER BY id DESC");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Frog Asia</title>
        <link rel="stylesheet" href="css/add-ons1.css">
        <link rel="stylesheet" href="css/main_style.css">
        <script src="js/jquery.min.js"></script>
        <script>
            function add_new_trans()
            {
                $("#mask_block").addClass('mask');
                $(".add_frog").show();
                $(".new_debitor_form").slideDown('slow');
            }

            function save_new_frog()
            {
                var frog_name = $("#frog_name").val();
                var gender = $("#gender").val();
                var live = $("#live").val();

                if (frog_name == '') {
                    alert('Please enter the frog name.');
                    $("#frog_name").focus();
                    return false;
                }
                if (frog_name != '') {
                    $.ajax
                            ({
                                type: "POST",
                                url: "save_frogs.php",
                                data: "add_frog=1&frog_name=" + frog_name + "&gender=" + gender + "&live=" + live,
                                beforeSend: loadStart,
                                complete: loadStop,
                                success: function(option)
                                {
                                    var options_divide = option.split('~');
                                    if (options_divide[0] == true) {
                                        $(".new_debitor_form").slideUp('slow');
                                        $("#mask_block").removeClass('mask');
                                        $(".add_frog").slideUp('slow');
                                        $("#frogs_data").html(options_divide[1]);
                                        $("#frog_name").val('');
                                    }
                                }
                            });
                }
            }

            function cancel_frog()
            {
                $(".new_debitor_form").slideUp('slow');
                $("#mask_block").removeClass('mask');
                $(".add_frog").slideUp('slow');
            }

            function delete_frog(ID)
            {
                var do_delete = confirm('Are you sure?');
                if (do_delete == true) {
                    $.ajax
                            ({
                                type: "POST",
                                url: "save_frogs.php",
                                data: "delete_frog=1&frog_id=" + ID,
                                beforeSend: loadStart,
                                complete: loadStop,
                                success: function(option)
                                {
                                    var options_divide = option.split('~');
                                    if (options_divide[0] == true) {
                                        $("#frogs_data").html(option);
                                    }
                                }
                            });
                }
            }

            function edit_frog(ID)
            {
                $(".frog_name_" + ID).hide();
                $(".frog_name_txt_" + ID).show();

                $(".frog_gender_" + ID).hide();
                $(".frog_gender_txt_" + ID).show();

                $(".frog_live_" + ID).hide();
                $(".frog_live_txt_" + ID).show();

                $(".edit_frog_item_" + ID).hide();
                $(".delete_frog_item_" + ID).hide();
                $(".update_frog_item_" + ID).show();
            }


            function update_frog(ID)
            {
                var frog_name = $(".frog_name_txt_" + ID).val();
                var frog_gender = $(".frog_gender_txt_" + ID).val();
                var frog_live = $(".frog_live_txt_" + ID).val();
                
                if (frog_name != '') {
                    $.ajax
                            ({
                                type: "POST",
                                url: "save_frogs.php",
                                data: "edit_frog=1&frog_name=" + frog_name + "&gender=" + frog_gender + "&live=" + frog_live+"&frog_id="+ID,
                                beforeSend: loadStart,
                                complete: loadStop,
                                success: function(option)
                                {
                                    if(option == true){
                                    $(".frog_name_" + ID).html(frog_name);
                                    $(".frog_name_" + ID).show();
                                    $(".frog_name_txt_" + ID).hide();
                                    
                                    var frog_gender_set = (frog_gender == 'M') ? "Male" : "Female";
                                    $(".frog_gender_" + ID).html(frog_gender_set);
                                    $(".frog_gender_" + ID).show();
                                    $(".frog_gender_txt_" + ID).hide();
                                    
                                    var frog_live_set = (frog_live == '1') ? "Live" : "Dead";
                                    $(".frog_live_" + ID).html(frog_live_set);
                                    $(".frog_live_" + ID).show();
                                    $(".frog_live_txt_" + ID).hide();

                                    $(".edit_frog_item_" + ID).show();
                                    $(".delete_frog_item_" + ID).show();
                                    $(".update_frog_item_" + ID).hide();
                                    }
                                }
                            });
                }
            }

            function live_filter()
            {
                var live_filter = $("#live_filter").val();
                if (live_filter != '') {
                    $.ajax
                            ({
                                type: "POST",
                                url: "save_frogs.php",
                                data: "live_filter=1&filter_by=" + live_filter,
                                beforeSend: loadStart,
                                complete: loadStop,
                                success: function(option)
                                {
                                    $("#frogs_data").html(option);
                                }
                            });
                }
            }

            function loadStart() {
                $('#loading').show();
            }

            function loadStop() {
                $('#loading').hide();
            }

            function go_home()
            {
                window.location = 'index.php';
            }
        </script>        
    </head>
    <body>
        <div id="loading"  style="position: fixed;top: 40%;left: 45%;padding: 5px;z-index: 1000;display: none;">
            <img src="images/preloader.gif" border="0" style="width: 48px;height: 48px;" />
        </div>
        <div id="mask_block" class="">

            <div class="add_frog">

                <div class="new_debitor_form">                    
                    <ul>
                        <div class="frog_title">
                            Add New Frog
                        </div>
                        <li>
                            <label>Frog Name</label>
                            <input class="text_box" type="text" name="frog_name" id="frog_name" />
                        </li>
                        <li>
                            <label>Gender</label>
                            <select id="gender">
                                <option value="M">Male</option>
                                <option value="F">Female</option>          
                            </select>
                        </li>
                        <li>
                            <label>Live Status</label>                            
                            <select id="live">
                                <option value="1">Live</option>
                                <option value="0">Dead</option>          
                            </select>
                        </li>
                        <li>
                            <label>&nbsp;</label>
                            <button class="action" onclick="return save_new_frog();">SAVE</button>
                            <button class="create" onclick="return cancel_frog();">CANCEL</button>
                        </li>
                    </ul>
                </div>


            </div>

        </div>

        <?php
        include './header.php';
        ?>

        <div id="all_data_grid">
            <table>
                <tr>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Live Status</th>
                    <th>Created Date</th>
                    <th>Actions</th>
                </tr>
            </table>
            <table id="frogs_data">
                <?php
                foreach ($frogs as $all_frogs) {
                    $gender = ($all_frogs['gender'] == "M") ? "Male" : "Female";
                    $live = ($all_frogs['live_status'] == "1") ? "Live" : "Dead";
                    ?>
                    <tr>
                        <td>
                            <span class="frog_name_<?php echo $all_frogs['id']; ?>"><?php echo $all_frogs['name']; ?></span>
                            <input class="text_box frog_name_txt_<?php echo $all_frogs['id']; ?> none" type="text" name="frog_name" id="frog_name" value="<?php echo $all_frogs['name']; ?>" />
                        </td>
                        <td>
                            <span class="frog_gender_<?php echo $all_frogs['id']; ?>"><?php echo $gender; ?></span>
                            <select class="frog_gender_txt_<?php echo $all_frogs['id']; ?> none" id="gender">
                                <option value="M" <?php if ($all_frogs['gender'] == "M") { ?> selected="selected" <?php } ?>>Male</option>
                                <option value="F" <?php if ($all_frogs['gender'] == "F") { ?> selected="selected" <?php } ?>>Female</option>          
                            </select>
                        </td>
                        <td>
                            <span class="frog_live_<?php echo $all_frogs['id']; ?>"><?php echo $live; ?></span>
                            <select class="frog_live_txt_<?php echo $all_frogs['id']; ?> none" id="live">
                                <option value="1" <?php if ($all_frogs['live_status'] == "1") { ?> selected="selected" <?php } ?>>Live</option>
                                <option value="0" <?php if ($all_frogs['live_status'] == "0") { ?> selected="selected" <?php } ?>>Dead</option>          
                            </select>
                        </td>
                        <td>
                            <?php echo $all_frogs['created']; ?>
                        </td>
                        <td>
                            <span class="edit_frog edit_frog_item_<?php echo $all_frogs['id']; ?>" onclick="edit_frog(<?php echo $all_frogs['id']; ?>);">EDIT |</span>
                            <span class="delete_frog delete_frog_item_<?php echo $all_frogs['id']; ?>" onclick="delete_frog(<?php echo $all_frogs['id']; ?>);">DELETE</span>                             
                            <span class="update_frog update_frog_item_<?php echo $all_frogs['id']; ?>" onclick="update_frog(<?php echo $all_frogs['id']; ?>);">UPDATE</span>
                        </td>
                    </tr>               
                    <?php
                }
                ?> 
            </table>
        </div> 

    </body>
</html>
