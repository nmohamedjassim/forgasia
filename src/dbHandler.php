<?php

class DbHandler {

    private $conn;

    function __construct() {
        require_once 'dbConnect.php';
        // opening db connection
        $db = new dbConnect();
        $this->conn = $db->connect();
    }

    /**
     * Fetching single record
     */
    public function getOneRecord($query) {
        $r = $this->conn->query($query . ' LIMIT 1') or die($this->conn->error . __LINE__);
        return $result = $r->fetch_assoc();
    }

    public function getAllRecord($query) {
        $r = $this->conn->query($query) or die($this->conn->error . __LINE__);
        foreach ($r as $value) {
            $val[] = $value;
        }
        if (sizeof($val) > 0) {
            return $val;
        } else {
            return '0';
        }
    }

    public function deleleRecord($query) {
        $r = $this->conn->query($query) or die($this->conn->error . __LINE__);
        if ($r) {
            return '1';
        }
    }
    
    public function updateRecord($query) {
        $r = $this->conn->query($query) or die($this->conn->error . __LINE__);
        if ($r) {
            return '1';
        }
    }

    /**
     * Creating new record
     */
    public function insertIntoTable() {        
        $query = "INSERT INTO frogs_main SET name       = '" . $_REQUEST['frog_name'] . "',
                                     gender             = '" . $_REQUEST['gender'] . "',
                                     live_status        = '" . $_REQUEST['live'] . "',
                                     created            = now(),
                                     updated            = now() ";
        $r = $this->conn->query($query) or die($this->conn->error . __LINE__);
        if ($r) {
            return '1';
        }
    }

    /**
     * Check string in command line
     */
    public function contains($str, $contain) {
        if (stripos($contain, "|") !== false) {
            $s = preg_split('/[|]+/i', $contain);
            $len = sizeof($s);
            for ($i = 0; $i < $len; $i++) {
                if (stripos($str, $s[$i]) !== false) {
                    return(true);
                }
            }
        }
        if (stripos($str, $contain) !== false) {
            return(true);
        }
        return(false);
    }

}

?>
