-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 11, 2016 at 03:14 PM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `frogs`
--

-- --------------------------------------------------------

--
-- Table structure for table `frogs_main`
--

CREATE TABLE IF NOT EXISTS `frogs_main` (
  `id` int(14) NOT NULL AUTO_INCREMENT,
  `name` varchar(124) NOT NULL,
  `gender` varchar(24) NOT NULL,
  `live_status` enum('0','1') NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `frogs_main`
--

INSERT INTO `frogs_main` (`id`, `name`, `gender`, `live_status`, `created`, `updated`) VALUES
(1, 'Test_frog', 'M', '1', '2016-04-11 13:07:05', '2016-04-11 13:07:05'),
(2, 'Test_From_Monday', 'F', '1', '2016-04-11 13:07:19', '2016-04-11 13:07:19'),
(3, 'Test_Frog_2', 'M', '0', '2016-04-11 13:07:35', '2016-04-11 13:07:35');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
