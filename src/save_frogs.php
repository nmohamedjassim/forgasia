<?php
require_once './dbHandler.php';
$db = new DbHandler();

if ($_POST['add_frog'] == '1') {
    $r = $db->insertIntoTable();
    if ($r == '1') {
        echo '1~';
        $frogs = $db->getAllRecord("select * from frogs_main ORDER BY id DESC");
        foreach ($frogs as $all_frogs) {
            $gender = ($all_frogs['gender'] == "M") ? "Male" : "Female";
            $live = ($all_frogs['live_status'] == "1") ? "Live" : "Dead";
            ?>
            <tr>
                <td>
                    <span class="frog_name_<?php echo $all_frogs['id']; ?>"><?php echo $all_frogs['name']; ?></span>
                    <input class="text_box frog_name_txt_<?php echo $all_frogs['id']; ?> none" type="text" name="frog_name" id="frog_name" value="<?php echo $all_frogs['name']; ?>" />
                </td>
                <td>
                    <span class="frog_gender_<?php echo $all_frogs['id']; ?>"><?php echo $gender; ?></span>
                    <select class="frog_gender_txt_<?php echo $all_frogs['id']; ?> none" id="gender">
                        <option value="M" <?php if ($all_frogs['gender'] == "M") { ?> selected="selected" <?php } ?>>Male</option>
                        <option value="F" <?php if ($all_frogs['gender'] == "F") { ?> selected="selected" <?php } ?>>Female</option>          
                    </select>
                </td>
                <td>
                    <span class="frog_live_<?php echo $all_frogs['id']; ?>"><?php echo $live; ?></span>
                    <select class="frog_live_txt_<?php echo $all_frogs['id']; ?> none" id="live">
                        <option value="1" <?php if ($all_frogs['live_status'] == "1") { ?> selected="selected" <?php } ?>>Live</option>
                        <option value="0" <?php if ($all_frogs['live_status'] == "0") { ?> selected="selected" <?php } ?>>Dead</option>          
                    </select>
                </td>
                <td>
                    <?php echo $all_frogs['created']; ?>
                </td>
                <td>
                    <span class="edit_frog edit_frog_item_<?php echo $all_frogs['id']; ?>" onclick="edit_frog(<?php echo $all_frogs['id']; ?>);">EDIT |</span>
                    <span class="delete_frog delete_frog_item_<?php echo $all_frogs['id']; ?>" onclick="delete_frog(<?php echo $all_frogs['id']; ?>);">DELETE</span>                             
                    <span class="update_frog update_frog_item_<?php echo $all_frogs['id']; ?>" onclick="update_frog(<?php echo $all_frogs['id']; ?>);">UPDATE</span>
                </td>
            </tr>
            <?php
        }
    }
}

if ($_POST['live_filter'] == '1') {
    $filter_by = $_POST["filter_by"];
    if ($filter_by != "ALL") {
        $frogs = $db->getAllRecord("select * from frogs_main WHERE live_status = '" . $filter_by . "' ORDER BY id DESC");
    } else {
        $frogs = $db->getAllRecord("select * from frogs_main ORDER BY id DESC");
    }
    foreach ($frogs as $all_frogs) {
        $gender = ($all_frogs['gender'] == "M") ? "Male" : "Female";
        $live = ($all_frogs['live_status'] == "1") ? "Live" : "Dead";
        ?>
        <tr>
            <td>
                <span class="frog_name_<?php echo $all_frogs['id']; ?>"><?php echo $all_frogs['name']; ?></span>
                <input class="text_box frog_name_txt_<?php echo $all_frogs['id']; ?> none" type="text" name="frog_name" id="frog_name" value="<?php echo $all_frogs['name']; ?>" />
            </td>
            <td>
                <span class="frog_gender_<?php echo $all_frogs['id']; ?>"><?php echo $gender; ?></span>
                <select class="frog_gender_txt_<?php echo $all_frogs['id']; ?> none" id="gender">
                    <option value="M" <?php if ($all_frogs['gender'] == "M") { ?> selected="selected" <?php } ?>>Male</option>
                    <option value="F" <?php if ($all_frogs['gender'] == "F") { ?> selected="selected" <?php } ?>>Female</option>          
                </select>
            </td>
            <td>
                <span class="frog_live_<?php echo $all_frogs['id']; ?>"><?php echo $live; ?></span>
                <select class="frog_live_txt_<?php echo $all_frogs['id']; ?> none" id="live">
                    <option value="1" <?php if ($all_frogs['live_status'] == "1") { ?> selected="selected" <?php } ?>>Live</option>
                    <option value="0" <?php if ($all_frogs['live_status'] == "0") { ?> selected="selected" <?php } ?>>Dead</option>          
                </select>
            </td>
            <td>
                <?php echo $all_frogs['created']; ?>
            </td>
            <td>
                <span class="edit_frog edit_frog_item_<?php echo $all_frogs['id']; ?>" onclick="edit_frog(<?php echo $all_frogs['id']; ?>);">EDIT |</span>
                <span class="delete_frog delete_frog_item_<?php echo $all_frogs['id']; ?>" onclick="delete_frog(<?php echo $all_frogs['id']; ?>);">DELETE</span>                             
                <span class="update_frog update_frog_item_<?php echo $all_frogs['id']; ?>" onclick="update_frog(<?php echo $all_frogs['id']; ?>);">UPDATE</span>
            </td>
        </tr>
        <?php
    }
}

if ($_POST['delete_frog'] == '1') {
    $frog_id = $_POST["frog_id"];
    $r = $db->deleleRecord("DELETE FROM frogs_main WHERE id = '" . $frog_id . "'");
    if ($r == '1') {
        echo '1~';
        $frogs = $db->getAllRecord("select * from frogs_main ORDER BY id DESC");
        foreach ($frogs as $all_frogs) {
            $gender = ($all_frogs['gender'] == "M") ? "Male" : "Female";
            $live = ($all_frogs['live_status'] == "1") ? "Live" : "Dead";
            ?>
            <tr>
                <td>
                    <span class="frog_name_<?php echo $all_frogs['id']; ?>"><?php echo $all_frogs['name']; ?></span>
                    <input class="text_box frog_name_txt_<?php echo $all_frogs['id']; ?> none" type="text" name="frog_name" id="frog_name" value="<?php echo $all_frogs['name']; ?>" />
                </td>
                <td>
                    <span class="frog_gender_<?php echo $all_frogs['id']; ?>"><?php echo $gender; ?></span>
                    <select class="frog_gender_txt_<?php echo $all_frogs['id']; ?> none" id="gender">
                        <option value="M" <?php if ($all_frogs['gender'] == "M") { ?> selected="selected" <?php } ?>>Male</option>
                        <option value="F" <?php if ($all_frogs['gender'] == "F") { ?> selected="selected" <?php } ?>>Female</option>          
                    </select>
                </td>
                <td>
                    <span class="frog_live_<?php echo $all_frogs['id']; ?>"><?php echo $live; ?></span>
                    <select class="frog_live_txt_<?php echo $all_frogs['id']; ?> none" id="live">
                        <option value="1" <?php if ($all_frogs['live_status'] == "1") { ?> selected="selected" <?php } ?>>Live</option>
                        <option value="0" <?php if ($all_frogs['live_status'] == "0") { ?> selected="selected" <?php } ?>>Dead</option>          
                    </select>
                </td>
                <td>
                    <?php echo $all_frogs['created']; ?>
                </td>
                <td>
                    <span class="edit_frog edit_frog_item_<?php echo $all_frogs['id']; ?>" onclick="edit_frog(<?php echo $all_frogs['id']; ?>);">EDIT |</span>
                    <span class="delete_frog delete_frog_item_<?php echo $all_frogs['id']; ?>" onclick="delete_frog(<?php echo $all_frogs['id']; ?>);">DELETE</span>                             
                    <span class="update_frog update_frog_item_<?php echo $all_frogs['id']; ?>" onclick="update_frog(<?php echo $all_frogs['id']; ?>);">UPDATE</span>
                </td>
            </tr>
            <?php
        }
    }
}

if($_POST['edit_frog'] == '1'){
    
    $frog_id        =    $_POST["frog_id"];
    $frog_name      =    $_POST['frog_name'];
    $frog_gender    =    $_POST['gender'];
    $frog_live      =    $_POST['live'];
    
    $r = $db->updateRecord("UPDATE frogs_main SET name = '".$frog_name."', gender = '".$frog_gender."', live_status = '".$frog_live."', created  = now() WHERE id = '" . $frog_id . "'");
     if ($r == '1') {
        echo '1';
     }
}


